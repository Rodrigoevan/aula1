package com.br.itau.controllers;

import com.br.itau.models.Matematica;
import com.br.itau.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PostMapping("soma")
    public Integer soma(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if(numeros.isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
            "E necessario ao menos 2 numeros para serem somados");
        }else{
            if (numeros.size() != 2){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "E necessario ao ter 2 numeros para serem somados");
            }
        }
        return matematicaService.soma(matematica.getNumeros());

    }
    @PostMapping("subtracao")
    public Integer subtracao(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if(numeros.isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "E necessario ao menos 2 numeros para serem subtraidos");
        }else{
            if (numeros.size() != 2){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "E necessario ao ter 2 numeros para serem subtraidos");
            }
        }
        return matematicaService.subtrair(matematica.getNumeros());

    }
    @PostMapping("multiplicacao")
    public Integer multiplicacao(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if(numeros.isEmpty() || numeros.size() < 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "E necessario ao menos 2 numeros para serem multiplicados");
        }else {
            if (numeros.size() != 2) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "E necessario ao ter 2 numeros para serem multiplicados");
            }
        }
        return matematicaService.multiplicar(matematica.getNumeros());

    }
    @PostMapping("divisao")
    public Double divisao(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if(numeros.isEmpty() || numeros.size() < 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "E necessario ao menos 2 numeros para serem divididos");
        }else {
            if (numeros.contains(0)){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "nao pode ter numero dividido por zero");

            }else {
                if (numeros.size() != 2) {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                            "E necessario ao ter 2 numeros para serem divididos");
                }
            }
        }
        if (numeros.get(0) < numeros.get(1)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "nao pode dividir um numero por um numero menor");
        }

        return matematicaService.dividir(matematica.getNumeros());

    }

}
