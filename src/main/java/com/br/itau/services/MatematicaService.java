package com.br.itau.services;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatematicaService {

    public  int soma(List<Integer> numeros){
        int resultado = numeros.get(0); int i = 0;
        for (int numero: numeros){
            if (i==0) {
                resultado = numero;
            }else{
                resultado += numero;
            }
            i++;
        }
        return resultado;
    }
    public  int subtrair(List<Integer> numeros){
        int resultado = numeros.get(0); int i = 0;
        for (int numero: numeros){
            if (i==0) {
                resultado = numero;
            }else{
                resultado -= numero;
            }
            i++;
        }
        return resultado;
    }
    public  int multiplicar(List<Integer> numeros){
        int resultado = numeros.get(0); int i = 0;
        for (int numero: numeros){
            if (i==0) {
                resultado = numero;
            }else{
                resultado *= numero;
            }
            i++;
        }
        return resultado;
    }
    public  Double dividir(List<Integer> numeros){
        Double resultado = numeros.get(0).doubleValue();     //setar o resultado pelo primeiro numero da lista
        int i = 0;
        for (int numero: numeros){
            if (i==0){
                resultado = Double.valueOf(numero);
            }else{
                resultado /= numero;
            }
            i++;
        }
        return resultado;
    }
}
